import re
import argparse

__author__ = 'scorvene'

"""
Added Subject to results at the request of Rachel Wise.
Added run config
"""

parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--input-file', dest='source', required=True)
parser.add_argument('--output-file', dest='destination', required=True)
arguments = parser.parse_args()

inputFile = arguments.source
outputFile = arguments.destination

email_file = open(inputFile, 'r')
temp_file = open("C:/Users/scorvene/Documents/HC Transcript requests/temp.txt", 'w')
final_file = open(outputFile, 'w')

topic_unstripped = '7. Research Topic = NULL\n'

for line in email_file:
    if line.startswith('Sent'):
        temp_file.write(line)
    if line.startswith('Subject'):
        temp_file.write(line)
    if line.startswith('1. '):
        temp_file.write(line)
    if line.startswith('2. '):
        temp_file.write(line)
    if line.startswith('3. '):
        temp_file.write(line)
    if line.startswith('4. '):
        temp_file.write(line)
    if line.startswith('5. '):
        temp_file.write(line)
    if line.startswith('6'):
        temp_file.write(line)
        nextline = next(email_file)
        if nextline.startswith('7. '):
            temp_file.write(nextline)
        elif nextline.startswith('8. '):
            temp_file.write(topic_unstripped)
            temp_file.write(nextline)
        nextline = next(email_file)
        temp_file.write(nextline)

temp_file.close()

headerlist = ['Date', 'Request/Download', 'Transcript Downloaded in English', 'What led you to the website', 'Last Name',
              'First Name', 'Country of Residence', 'Institution', 'Topic', 'Email\n']

headers = ';'.join(headerlist)
final_file.write(headers)

temp = open("C:/Users/scorvene/Documents/HC Transcript requests/temp.txt", 'r')

for line in temp:
    if line.startswith('Sent'):
        # print line
        sent = line[-9:]
        # print sent
        sent_time = sent.strip()
        sentline = line.split(':')
        sent = sentline[1]
        line0 = sent.strip()
        line00 = line0[:-2]
        line000 = line00.split(',')
        year = line000[2].strip()
        monthDay = line000[1].strip()
        final_date = monthDay + ', ' + year

    if line.startswith('Subject'):
        subject_line_list = line.split(':')
        subject_unstripped = subject_line_list[1].strip()
        subject_truncated = re.sub('Interview Transcript ', '', subject_unstripped)
        email_subject = re.sub('\n', '', subject_truncated)

    if line.startswith("1."):
        name_line_list = line.split(' = ')
        name_unstripped = name_line_list[1]
        transcript_subject = re.sub('\n', '', name_unstripped)

    if line.startswith('2.'):
        why_line_list = line.split(' = ')
        why_unstripped = why_line_list[1]
        why = re.sub('\n', '', why_unstripped)

    if line.startswith('3.'):
        last_name_list = line.split(' = ')
        last_name_unstripped = last_name_list[1]
        last_name = re.sub('\n', '', last_name_unstripped)

    if line.startswith('4.'):
        first_name_list = line.split(' = ')
        first_name_unstripped = first_name_list[1]
        first_name = re.sub('\n', '', first_name_unstripped)

    if line.startswith('5.'):
        country_list = line.split(' = ')
        country_unstripped = country_list[1]
        country = re.sub('\n', '', country_unstripped)

    if line.startswith('6.'):
        institution_list = line.split(' = ')
        institution_unstripped = institution_list[1]
        institution = re.sub('\n', '', institution_unstripped)

    if line.startswith('7.'):
        topic_list = line.split(' = ')
        topic_unstripped = topic_list[1]
        topic = re.sub('\n', '', topic_unstripped)

    if line.startswith('8.'):
        email_line_list = line.split(' = ')
        email_list = email_line_list[1].split()
        email_unstripped = email_list[0]
        email = email_unstripped + '\n'

        request_list = list()
        request_list.append(final_date)
        request_list.append(email_subject)
        request_list.append(transcript_subject)
        request_list.append(why)
        request_list.append(last_name)
        request_list.append(first_name)
        request_list.append(country)
        request_list.append(institution)
        request_list.append(topic)
        request_list.append(email)

        request = ';'.join(request_list)

        final_file.write(request)
