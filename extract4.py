__author__ = 'scorvene'

"""
This version inserts line 7 if missing.
IT WORKS!
"""

import re
import argparse

parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--input-file', dest='source', required=True)
parser.add_argument('--output-file', dest='destination', required=True)
arguments = parser.parse_args()

inputFile = arguments.source
outputFile = arguments.destination

email_file = open(inputFile, 'r')
temp_file = open("C:/Users/scorvene/Documents/HC Transcript requests/temp.txt", 'w')
final_file = open(outputFile, 'w')

topic = '7. Research Topic = NULL\n'

for line in email_file:
    if line.startswith('Sent'):
        temp_file.write(line)
    if line.startswith('1. '):
        temp_file.write(line)
    if line.startswith('2. '):
        temp_file.write(line)
    if line.startswith('3. '):
        temp_file.write(line)
    if line.startswith('4. '):
        temp_file.write(line)
    if line.startswith('5. '):
        temp_file.write(line)
    if line.startswith('6'):
        # line = line.strip()
        temp_file.write(line)
        nextline = next(email_file)
        # print nextline
        if nextline.startswith('7. '):
            # nextline = nextline.strip()
            temp_file.write(nextline)
        elif nextline.startswith('8. '):
            temp_file.write(topic)
            temp_file.write(nextline)
        nextline = next(email_file)
        temp_file.write(nextline)

temp_file.close()

headerlist = ['Year', 'Month/Day', 'Time', 'Transcript Downloaded in English', 'What led you to the website', 'Last Name',
              'First Name', 'Country of Residence', 'Institution', 'Topic', 'Email\n']

headers = ';'.join(headerlist)
final_file.write(headers)

temp = open("C:/Users/scorvene/Documents/HC Transcript requests/temp.txt", 'r')

for line in temp:
    if line.startswith('Sent'):
        sent = line[-9:]
        sent_time = sent.strip()
        sentline = line.split(':')
        sent = sentline[1]
        line0 = sent.strip()
        line00 = line0[:-2]
        line000 = line00.split(',')
        year = line000[2].strip()
        monthDay = line000[1].strip()

    if line.startswith("1."):
        nameline = line.split(' = ')
        name = nameline[1]
        line1 = re.sub('\n', '', name)

    if line.startswith('2.'):
        whyline = line.split(' = ')
        why = whyline[1]
        line2 = re.sub('\n', '', why)

    if line.startswith('3.'):
        lastline = line.split(' = ')
        last = lastline[1]
        line3 = re.sub('\n', '', last)

    if line.startswith('4.'):
        firstline = line.split(' = ')
        first = firstline[1]
        line4 = re.sub('\n', '', first)

    if line.startswith('5.'):
        countryline = line.split(' = ')
        country = countryline[1]
        line5 = re.sub('\n', '', country)

    if line.startswith('6.'):
        institutionline = line.split(' = ')
        institution = institutionline[1]
        line6 = re.sub('\n', '', institution)

    if line.startswith('7.'):
        topicline = line.split(' = ')
        topic = topicline[1]
        line7 = re.sub('\n', '', topic)

    if line.startswith('8.'):
        emailline = line.split(' = ')
        email_list = emailline[1].split()
        email = email_list[0]
        line8 = email + '\n'

        request_list = []
        request_list.append(year)
        request_list.append(monthDay)
        request_list.append(sent_time)
        request_list.append(line1)
        request_list.append(line2)
        request_list.append(line3)
        request_list.append(line4)
        request_list.append(line5)
        request_list.append(line6)
        request_list.append(line7)
        request_list.append(line8)

        request = ';'.join(request_list)

        final_file.write(request)
